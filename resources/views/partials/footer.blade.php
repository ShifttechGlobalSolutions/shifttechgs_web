<footer id="rs-footer" class="rs-footer style1">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10">
                    <div class="footer-logo mb-40">
                        <a href="index-2.html"><img src="{{asset('public/images/shift_logo.png')}}" alt=""></a>
                    </div>
                    <div class="textwidget white-color pb-40"><p>We Are A Group Of Software Developers Who Are Dedicated To Exceeding Our Clients Expectations.</p>
                    </div>
                    <ul class="footer-social md-mb-30">

                        <li>
                            <a href="https://www.linkedin.com/company/shifttech-global-solutions " target="_blank"><span><i class="fa fa-linkedin"></i></span></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/shifttechgs/" target="_blank"><span><i class="fa fa-instagram"></i></span></a>
                        </li>

                    </ul>
                </div>


                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10 pl-55 md-pl-15">
                    <h3 class="footer-title">Our Services</h3>
                    <ul class="site-map">
                        <li><a href="#rs-services">Custom Software Development</a></li>
                        <li><a href="#rs-services">Website Development</a></li>
                        <li><a href="#rs-services">Mobile Apps Development</a></li>

                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10">
                    <h3 class="footer-title">Contact Info</h3>
                    <ul class="address-widget">
                        <li>
                            <i class="flaticon-location"></i>
                            <div class="desc">1 Stepney Rd, Unit H1, Parklands, <br>
                                CapeTown, South Africa</div>
                        </li>
                        <li>
                            <i class="flaticon-call"></i>
                            <div class="desc">
                                <a href="tel:(+27)814303023">(+27) 814 303 023</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-email"></i>
                            <div class="desc">
                                <a href="mailto:admin@shifttechgs.com">admin@shifttechgs.com</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-clock-1"></i>
                            <div class="desc">
                                Office Hours: 8AM - 5PM
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row align-content-center">

                <div class="col-lg-12">
                    <div class="copyright text-lg-start text-center ">

                        <p>Copyright ©<script>document.write(new Date().getFullYear());</script> Shifttech Global Solutions -  Software Development Services | Software Solutions | Mobile App Development</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp" class="orange-color">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->


