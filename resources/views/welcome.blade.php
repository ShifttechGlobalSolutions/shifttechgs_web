@extends("layouts.master")
@section("content")


    <!-- Main content Start -->
    <div class="main-content">

        <!--Full width header Start-->

        <!--Full width header End-->

        <!-- Banner Section Start -->
        <div class="rs-banner main-home">
            <div class="container custom">
                <div class="content-wrap">
                    <div class="border-line"></div>
                    <h1 class="title">A Custom Software </h1>
                    <h3 class="sub-title">Development Company</h3>
                    <div class="btn-part">
                        <a class="readon consultant" href="rs-contact">Get in touch</a>
                    </div>

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="animate-part">
                <div class="spiner dot">
                    <img class="rotate_Y" src="{{asset('public/images/banner/style1/dot-rect.png')}}" alt="images">
                </div>
                <div class="spiner tri-circle">
                    <img class="up-down" src="{{asset('public/images/samples/tria.png')}}" alt="images">
                </div>
                <div class="spiner circle">
                    <img class="up-down" src="{{asset('public/images/samples/circle2..png')}}" alt="images">
                </div>
                <div class="spiner line">
                    <img class="up-down" src="{{asset('public/images/banner/style1/line1.png')}}" alt="images">
                </div>
            </div>
        </div>
        <!-- Banner Section End -->



        <!-- About Section Start -->
        <div id="rs-about" class="rs-about style2 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 pr-33 md-pr-15 md-mb-50">
                        <div class="images-part">
                            <img src="{{asset('public/images/samples/abt12.jpg')}}" alt="Images">
                        </div>
                    </div>
                    <div class="col-lg-6 ">
                        <div class="sec-title">
                            <h2 class="title pb-22">
                                We help to implement your ideas into automation
                            </h2>
                            <span class="sub-text big">We are a group of software developers who are dedicated to exceeding our clients expectations.</span>

                            <p class="margin-0 pt-15">Shifttech Global Solutions is a custom software development company providing a complete software design and development service. Shifttech Global Solutions delivers the best in class custom software solutions, elite software development teams and innovative cloud software to enterprise business across numerous industries.

                                We believe new technologies are the lifeline of every business in the modern age and aim to connect businesses across all industries to innovative software, technological development, solutions and services, in a manner that’s faster, easier and better than ever before.<br>

                                At Shifttech Global Solutions we have a vested interest in the success of your business.
                                Our vision is to align our service offerings in a manner that gives your business a competitive
                                edge in the market.</p>
                            <div class="btn-part mt-45 md-mt-30">
                                <a class="readon consultant discover" href="#rs-contact">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rs-animation">
                <div class="animate-style">
                    <img class="scale" src="{{asset('public/images/samples/tria.png')}}" alt="About">
                </div>
            </div>
        </div>
        <!-- About Section End -->



        <!-- Premium Services Section Start -->
        <div id="rs-services" class="rs-services style2 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 d-flex align-items-center mb-60 md-mb-40">
                    <div class="first-half">
                        <div class="sub-text">What We Offer</div>
                        <h2 class="title mb-0 md-pb-20">The best solutions for your business - <span>what we do.</span></h2>
                    </div>
                    <div class="last-half">
                        <p class="desc mb-0 pl-20 md-pl-15">5+ years experience in Software development and IT consulting, and working with clients over the world.</p>
                    </div>
                </div>
                <div class="rs-carousel owl-carousel"
                     data-loop="false"
                     data-items="3"
                     data-margin="30"
                     data-autoplay="true"
                     data-hoverpause="true"
                     data-autoplay-timeout="5000"
                     data-smart-speed="800"
                     data-dots="true"
                     data-nav="false"
                     data-nav-speed="false"

                     data-md-device="3"
                     data-md-device-nav="false"
                     data-md-device-dots="true"
                     data-center-mode="false"

                     data-ipad-device2="2"
                     data-ipad-device-nav2="false"
                     data-ipad-device-dots2="true"

                     data-ipad-device="2"
                     data-ipad-device-nav="false"
                     data-ipad-device-dots="true"

                     data-mobile-device="1"
                     data-mobile-device-nav="false"
                     data-mobile-device-dots="true">

                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/samples/support.png')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a >Website Development</a></h3>
                            <div class="desc">A responsive and visually appealing website is crucial to brand recognition in this day of digital media.
                                At Shifttech we incorporate functionality and beauty with the latest technology to provide you with the best user experience available.</div>
                        </div>
                    </div>
                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/samples/dev.png')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a >Custom Software Solutions</a></h3>
                            <div class="desc">Software Solutions that make your business more efficient. At Shifttech our dedicated team of passionate developers think
                                outside the box to bring your product or platform to market, quickly and cost-efficiently.
                                </div>
                        </div>
                    </div>
                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/samples/mobile.png')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a >Mobile Apps Development</a></h3>
                            <div class="desc">From conceptualization to deployment, we build mobile and web applications to suit your customer’s needs.
                                Our software developers always design with the user in mind. </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Premium Services Section End -->



        <!-- Services Section Start -->
        <div class="rs-services home-style2  pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 text-center md-left mb-40">
                    <div class="sub-text">Why Choose Us</div>
                    <h2 class="title">Get our services & turn your ideas into <br><span>reality.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-4  md-mb-50 pr-30 md-pr-l5">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >Agile Software Development</a></h3>
{{--                                <p>Our Shifttech projects managers implement an agile methodology to allow scalability and deliverability.</p>--}}
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >Trusted experience</a></h3>
{{--                                <p>Shifttech has been building custom software solutions since 2018. We have the knowledge and experience you require to take your idea to the next level.</p>--}}

                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >UX & UI</a></h3>
{{--                                <p>User experience and functionality are what we pride ourselves on, we design our solutions to meet customer demands.</p>--}}

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4  md-mb-50">
                        <div class="rs-videos choose-video">
                            <div class="images-video">
                                <img src="{{asset('public/images/samples/devv.jpg')}}" alt="images">
                            </div>
                            <div class="animate-border">
                                <a class="popup-border" href="https://www.youtube.com/watch?v=zi7uGg6FVM4">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 pl-40 md-pl-15">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >Dedicated DevOps Teams</a></h3>
{{--                                <p>Our teams of developers are assigned development projects allowing for efficient end-to-end deliverability.</p>--}}
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >Platform maintenance and support</a></h3>
{{--                                <p>Our dedicated teams of developers ensure fast turnaround times for content updates, security and troubleshooting.</p>--}}
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/samples/shf.jpg')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a >Transparency</a></h3>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section End -->

            <!-- Counter Section End -->
        </div>
        <!-- Services Section End -->

        <!-- Process Section Start -->
        <div class="rs-process style1 black-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-5">
                        <div class="sec-title2 md-text-center">
                            <div class="sub-text">Working Process</div>
                            <h2 class="title mb-23 white-color">How we work for our valued  <span>customers.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="btn-part text-right md-text-center">
                            <a class="readon consultant discover" href="">View Works</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container custom2">
                <div class="process-effects-layer">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/1.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 1 </span></div>
                                        <div class="number-title"><h3 class="title"> STRATEGY</h3></div>
                                        <div class="number-txt">
                                            Initial Converstaion, Requirement Analysis & Define Scope
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/2.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 2 </span></div>
                                        <div class="number-title"><h3 class="title">Design</h3></div>
                                        <div class="number-txt">
                                            User flows, Wireframes, Brand Integration, Mockups & Prototypes and Evaluations.                                     </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 sm-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/3.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 3 </span></div>
                                        <div class="number-title"><h3 class="title">Development & Testing</h3></div>
                                        <div class="number-txt">
                                            Architecture, coding, DB Design, API, Weekly sprints.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/4.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 4 </span></div>
                                        <div class="number-title"><h3 class="title"> Deployment</h3></div>
                                        <div class="number-txt">
                                            Deployment plan ,Release to Production & Ongoing maintenance.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Process Section End -->

        <!-- Project Section Start -->
        <div id="rs-project" class="rs-project style7 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container custom">
                <div class="row y-middle">
                    <div class="col-lg-6">
                        <div class="sec-title2 mb-55 md-mb-30">
                            <div class="sub-text">Recent Work</div>
                            <h2 class="title mb-23">We blend business ideas to create something <span>awesome.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-60 md-pl-15 mb-30">
                        <p class="desc mb-0">5+ years experience in Software development and IT consulting, and working with clients over the world.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid pl-30 pr-30">
                <div class="rs-carousel owl-carousel"
                     data-loop="true"
                     data-items="4"
                     data-margin="30"
                     data-autoplay="false"
                     data-hoverpause="true"
                     data-autoplay-timeout="5000"
                     data-smart-speed="800"
                     data-dots="false"
                     data-nav="false"
                     data-nav-speed="false"

                     data-md-device="4"
                     data-md-device-nav="false"
                     data-md-device-dots="true"
                     data-center-mode="false"

                     data-ipad-device2="2"
                     data-ipad-device-nav2="false"
                     data-ipad-device-dots2="true"

                     data-ipad-device="2"
                     data-ipad-device-nav="false"
                     data-ipad-device-dots="true"

                     data-mobile-device="1"
                     data-mobile-device-nav="false"
                     data-mobile-device-dots="true">

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/aaaaaa.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://www.balanced-view.com/">Balanced-View Ltd</a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/ggg.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://dubairugby7s.com/">Emirates Dubai 7s</a>
                        </div>
                    </div>

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/dubs.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://play.google.com/store/apps/details?id=com.back9solutions.eventapp.dubai7s">Emirates Dubai 7s App</a>
                        </div>
                    </div>

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/ray.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://rayandsons.co.za/">Ray and Sons</a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/bbbb.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://www.shifttechgs.com/springkleaners/">Spring Kleaners</a>
                        </div>
                    </div>

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/ffffff.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://www.deltecenergysolutions.co.za/">Deltec Energy</a>
                        </div>
                    </div>

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/samples/furniture.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">

                            <a class="p-icon" href="https://www.furniturespot.co.za/">FurnitureSpot</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Project Section End -->



        <!-- Blog Start -->
        <div id="rs-stack" class="rs-blog style2 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row y-middle md-mb-30">
                    <div class="col-lg-5 mb-20 md-mb-10">
                        <div class="sec-title2">
                            <div class="sub-text">Our Stack</div>
                            <h2 class="title mb-23">Our Tech Stack  <span>Knowledge.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-7 mb-20">
                        <div class="btn-part text-right md-left">
                            <a class="readon consultant discover" href="#rs-contact">Get Started</a>
                        </div>
                    </div>
                </div>
                <div class="rs-patter-section style1 pt-75">
                    <div class="container custom">
                        <div class="rs-carousel owl-carousel"
                             data-loop="true"
                             data-items="5"
                             data-margin="30"
                             data-autoplay="true"
                             data-hoverpause="true"
                             data-autoplay-timeout="5000"
                             data-smart-speed="800"
                             data-dots="false"
                             data-nav="false"
                             data-nav-speed="false"

                             data-md-device="5"
                             data-md-device-nav="false"
                             data-md-device-dots="false"
                             data-center-mode="false"

                             data-ipad-device2="4"
                             data-ipad-device-nav2="false"
                             data-ipad-device-dots2="true"

                             data-ipad-device="3"
                             data-ipad-device-nav="false"
                             data-ipad-device-dots="false"

                             data-mobile-device="2"
                             data-mobile-device-nav="false"
                             data-mobile-device-dots="false">
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/copy.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/copy.png')}}" title="" alt="">
                                    <p>Angular</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/flutter.jpg')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/flutter.jpg')}}" title="" alt="">
                                    <p>Flutter</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/android.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/android.png')}}" title="" alt="">
                                    <p>Android</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/laravel.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/laravel.png')}}" title="" alt="">
                                    <p>Laravel</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/net.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/net.png')}}" title="" alt="">
                                    <p>.Net Core</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/node.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/node.png')}}" title="" alt="">
                                    <p>Nodejs</p>
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/vue.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/vue.png')}}" title="" alt="">
                                    <p>Vue</p>
                                </a>
                            </div>

                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/mongo.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/mongo.png')}}" title="" alt="">
                                    <p>MongoDB</p>
                                </a>
                            </div>

                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/sql.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/sql.png')}}" title="" alt="">
                                    <p>SQL</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->


{{--        <!-- Blog Start -->--}}
{{--        <div id="rs-stack" class="rs-blog style2 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">--}}
{{--            <div class="container">--}}
{{--                <div class="row y-middle md-mb-30">--}}
{{--                    <div class="col-lg-5 mb-20 md-mb-10">--}}
{{--                        <div class="sec-title2">--}}
{{--                            <div class="sub-text">Clients</div>--}}
{{--                            <h2 class="title mb-23">Our Trusted   <span>Clients.</span></h2>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-7 mb-20">--}}
{{--                        <div class="btn-part text-right md-left">--}}
{{--                            <a class="readon consultant discover" href="#rs-contact">Get Started</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="rs-patter-section style1 pt-75">--}}
{{--                    <div class="container custom">--}}
{{--                        <div class="rs-carousel owl-carousel"--}}
{{--                             data-loop="true"--}}
{{--                             data-items="5"--}}
{{--                             data-margin="30"--}}
{{--                             data-autoplay="true"--}}
{{--                             data-hoverpause="true"--}}
{{--                             data-autoplay-timeout="5000"--}}
{{--                             data-smart-speed="800"--}}
{{--                             data-dots="false"--}}
{{--                             data-nav="false"--}}
{{--                             data-nav-speed="false"--}}

{{--                             data-md-device="5"--}}
{{--                             data-md-device-nav="false"--}}
{{--                             data-md-device-dots="false"--}}
{{--                             data-center-mode="false"--}}

{{--                             data-ipad-device2="4"--}}
{{--                             data-ipad-device-nav2="false"--}}
{{--                             data-ipad-device-dots2="true"--}}

{{--                             data-ipad-device="3"--}}
{{--                             data-ipad-device-nav="false"--}}
{{--                             data-ipad-device-dots="false"--}}

{{--                             data-mobile-device="2"--}}
{{--                             data-mobile-device-nav="false"--}}
{{--                             data-mobile-device-dots="false">--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/2ship.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/2ship.png')}}" title="" alt="">--}}
{{--                                    <p>2Ship</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/luna.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/luna.png')}}" title="" alt="">--}}
{{--                                    <p>Lunasoft</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/ray_lo.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/ray_lo.png')}}" title="" alt="">--}}
{{--                                    <p>RayAndSons</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/olympia.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/olympia.png')}}" title="" alt="">--}}
{{--                                    <p>Olympia</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/dubs.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/dubs.png')}}" title="" alt="">--}}
{{--                                    <p>Dubai7s</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/deltec_lo.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/deltec_lo.png')}}" title="" alt="">--}}
{{--                                    <p>Deltec Batteries</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/spring_logoo.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/spring_logoo.png')}}" title="" alt="">--}}
{{--                                    <p>Spring Kleaners</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}

{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/furnitur.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/furnitur.png')}}" title="" alt="">--}}
{{--                                    <p>FurnitureSpot</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}

{{--                            <div class="logo-img">--}}
{{--                                <a href="https://rstheme.com/">--}}
{{--                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/samples/cmetv_lo.png')}}" title="" alt="">--}}
{{--                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/samples/cmetv_lo.png')}}" title="" alt="">--}}
{{--                                    <p>CMETV</p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- Blog End -->--}}



        <!-- Contact Section Start -->
        <div id="rs-contact" class="rs-contact contact-style2 gray-bg  pt-95 pb-100 md-pt-65 md-pb-70">
            <div class="container">
                <div class="sec-title2 mb-55 md-mb-35 text-center text-lg-start">
                    <div class="sub-text">Contact</div>
                    <h2 class="title mb-0">Let us help your business <br> to move <span>forward.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-50">
                        <div class="contact-img">
                            <img src="{{asset('public/images/samples/contact.png')}}" alt="Contact">
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="contact-wrap">
                            <div id="form-messages"></div>
                            <form id="contactForm" class="contacts-form" method="post"
                                  action="{{ route('contact.store') }}">
                                @csrf
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="name" name="name" placeholder="Name" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="email" name="email" placeholder="E-Mail" required="">
                                        </div>

                                        <div class="col-lg-12 mb-30">
                                            <input class="from-control" type="text" id="subject" name="subject" placeholder="Subject" required="">
                                        </div>

                                        <div class="col-lg-12 mb-30">
                                            <textarea class="from-control" id="message" name="message" placeholder="Your message Here" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="btn-part">
                                        <div class="form-group mb-0">
                                            <input class="readon submit" type="submit" value="Submit Now">
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Section End -->

    </div>
    <!-- Main content End -->


@endsection
